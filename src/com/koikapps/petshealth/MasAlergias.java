package com.koikapps.petshealth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MasAlergias extends Activity {
	public String uriAPI = "http://koikapps.com/petshealth/web/insert/alergia.php";
	private Button continuar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alergias);

		continuar = (Button) findViewById(R.id.btnAddAlergia);
		continuar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sendPostRequest(uriAPI);
			}

		});
	}

	public void sendPostRequest(final String URL) {

		class MyPost extends AsyncTask<Void, Void, Void> {

			@Override
			protected Void doInBackground(Void... arg0) {
				// Create a new HttpClient and Post Header
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(URL);

				try {
					// Add your data

					EditText alergia = (EditText) findViewById(R.id.et_alergia);

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("mascota_id", getIntent().getExtras().getString("mascota_id").toString()));
					nameValuePairs.add(new BasicNameValuePair("alergia",
							alergia.getText().toString()));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					// Execute HTTP Post Request
					httpclient.execute(httppost);
					// Log.v("Post Status","Code: "+response.getStatusLine().getStatusCode());
				} catch (ClientProtocolException e) {
					Log.i("response", "nope" + e);
				} catch (IOException e) {
					Log.i("response", "nope" + e);
				}
				return null;

			}

			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				Toast.makeText(getApplicationContext(),
						R.string.guardado, Toast.LENGTH_LONG)
						.show();
				Intent intent2 = new Intent();
				intent2.setClass(MasAlergias.this, MiMascota.class);
				startActivity(intent2);

			}
		}

		MyPost sendPostReqAsyncTask = new MyPost();
		sendPostReqAsyncTask.execute();

	}
}
