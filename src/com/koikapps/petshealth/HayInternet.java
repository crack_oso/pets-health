package com.koikapps.petshealth;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class HayInternet {

	ConnectivityManager connectivityManager;
	NetworkInfo wifiInfo, mobileInfo;

	public Boolean revisarAhora(Context con) {

		try {
			connectivityManager = (ConnectivityManager) con
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			wifiInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			mobileInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			if (wifiInfo.isConnected() || mobileInfo.isConnected()) {
				//Toast.makeText(con, "Sí hay internet", Toast.LENGTH_LONG).show();
				return true;
			}
		} catch (Exception e) {
			Toast.makeText(con, "Revise su conexión a internet", Toast.LENGTH_LONG).show();
		}

		return false;
	}
}