package com.koikapps.petshealth;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class MasEnfermedades extends Activity {
	public String uriAPI = "http://koikapps.com/petshealth/web/insert/enfermedad.php";
	private Button continuar;
	private ImageView calendarioEnfermedades;
	private TextView tvDisplayDate;
	private int year, month, day;
	static final int DATE_DIALOG_ID = 999;
	String fecha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enfermedades);

		continuar = (Button) findViewById(R.id.btnAddEnfermedad);
		continuar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sendPostRequest(uriAPI);
			}
		});
		tvDisplayDate = (TextView) findViewById(R.id.fechaEnfermedades);
		calendarioEnfermedades = (ImageView) findViewById(R.id.calendarioEnfermedades);
		calendarioEnfermedades.setOnClickListener(new OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
		setCurrentDateOnView();
	}

	public void setCurrentDateOnView() {

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		StringBuilder currentDate = new StringBuilder().append(month + 1)
				.append("-").append(day).append("-").append(year);
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date myDate = null;
		try {
			myDate = dateFormat.parse(currentDate.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM-dd-yyyy");
		String finalDate = timeFormat.format(myDate);
		tvDisplayDate.setText(finalDate.toString());
		fecha = finalDate;

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			StringBuilder selectedDate = new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year);

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

			Date myDate = null;
			try {
				myDate = dateFormat.parse(selectedDate.toString());

			} catch (ParseException e) {
				e.printStackTrace();
			}
			SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM-dd-yyyy");
			String date = timeFormat.format(myDate);
			fecha = date;

			tvDisplayDate.setText(date);

		}
	};

	public void sendPostRequest(final String URL) {

		class MyPost extends AsyncTask<Void, Void, Void> {

			@Override
			protected Void doInBackground(Void... arg0) {
				// Create a new HttpClient and Post Header
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(URL);

				try {
					// Add your data

					EditText enfermedad = (EditText) findViewById(R.id.et_enfermedad);
					EditText tratamiento = (EditText) findViewById(R.id.et_tratamiento);

					Date lastDate = null;
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"MMMM-dd-yyyy");
					try {
						lastDate = dateFormat.parse(fecha.toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					SimpleDateFormat timeFormat = new SimpleDateFormat(
							"yyyy-MM-dd");
					String dateSended = timeFormat.format(lastDate);

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("mascota_id",
							getIntent().getExtras().getString("mascota_id")
									.toString()));
					nameValuePairs.add(new BasicNameValuePair("enfermedad",
							enfermedad.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("tratamiento",
							tratamiento.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("fecha",
							dateSended.toString()));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					httpclient.execute(httppost);
				} catch (ClientProtocolException e) {
					Log.i("response", "nope" + e);
				} catch (IOException e) {
					Log.i("response", "nope" + e);
				}
				return null;

			}

			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				Toast.makeText(getApplicationContext(), R.string.guardado,
						Toast.LENGTH_LONG).show();
				Intent intent2 = new Intent();
				intent2.setClass(MasEnfermedades.this, MiMascota.class);
				startActivity(intent2);

			}
		}

		MyPost sendPostReqAsyncTask = new MyPost();
		sendPostReqAsyncTask.execute();

	}
}