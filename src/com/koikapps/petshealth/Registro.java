package com.koikapps.petshealth;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registro extends Activity {

	public String uriAPI = "http://koikapps.com/petshealth/web/insert/user.php";
	private Button continuar;
	public EditText email, usuario, password, confirmacion, nombre;
	ProgressDialog pDialog;
	int codigoRespuesta = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);

		email = (EditText) findViewById(R.id.email_reg);
		password = (EditText) findViewById(R.id.pass2);
		confirmacion = (EditText) findViewById(R.id.confirm_pass);
		continuar = (Button) findViewById(R.id.button_continuar2);
		continuar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				boolean validmail = isEmailValid(email.getText().toString());
				if (validmail == true) {
					String passwd1Text = password.getText().toString();
					String passwd2Text = confirmacion.getText().toString();

					if (passwd1Text.equals(passwd2Text)) {
						if (passwd1Text.length() > 5) {
							if (passwd1Text.length() < 20) {
								pDialog = new ProgressDialog(Registro.this,
										R.style.progress_bar_style);
								pDialog.getWindow()
										.setBackgroundDrawable(
												new ColorDrawable(
														android.graphics.Color.TRANSPARENT));
								pDialog.setTitle(null);
								pDialog.setMessage(getString(R.string.CargandoTexto));
								pDialog.setIndeterminate(false);
								pDialog.setCancelable(false);
								pDialog.show();
								sendPostRequest(uriAPI);
							} else {
								Toast.makeText(getApplication(),
										R.string.masDe20Caracteres,
										Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(getApplication(),
									R.string.menosDe6Caracteres,
									Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getApplication(), R.string.noCoinciden,
								Toast.LENGTH_SHORT).show();
					}

				} else {
					Toast.makeText(getApplication(), R.string.emailInvalido,
							Toast.LENGTH_SHORT).show();
				}
			}

		});
	}

	public void sendPostRequest(final String URL) {

		class MyPost extends AsyncTask<Void, Void, Void> {

			@Override
			protected Void doInBackground(Void... arg0) {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(URL);

				String encryptedPass = md5(password.getText().toString());
				try {
					email = (EditText) findViewById(R.id.email_reg);
					usuario = (EditText) findViewById(R.id.user_reg);
					nombre = (EditText) findViewById(R.id.nombre_reg);
					String idiomaDispositivo = Locale.getDefault()
							.getDisplayLanguage();

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("email", email
							.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("usuario",
							usuario.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("nombre", nombre
							.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("password",
							encryptedPass));
					nameValuePairs.add(new BasicNameValuePair("version",
							"gratis"));
					nameValuePairs.add(new BasicNameValuePair("idioma",
							idiomaDispositivo));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					HttpResponse response = httpclient.execute(httppost);
					codigoRespuesta = response.getStatusLine().getStatusCode();

					Log.v("REspuesta del servidor", "" + codigoRespuesta);
				} catch (ClientProtocolException e) {
					Log.i("response", "nope" + e);
				} catch (IOException e) {
					Log.i("response", "nope" + e);
				}
				pDialog.dismiss();
				return null;

			}

			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if (codigoRespuesta == 200) {
					Toast.makeText(getApplicationContext(),
							R.string.yaPuedeIniciar, Toast.LENGTH_LONG).show();
					Intent intent2 = new Intent();
					intent2.setClass(Registro.this, IniciarSesion.class);
					startActivity(intent2);
				} else if (codigoRespuesta == 402) {
					Toast.makeText(Registro.this,
							R.string.CorreoOUsuarioDuplicado, Toast.LENGTH_LONG)
							.show();
				}
			}
		}

		MyPost sendPostReqAsyncTask = new MyPost();
		sendPostReqAsyncTask.execute();

	}

	public String md5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

			return hexString.toString();
		}

		catch (NoSuchAlgorithmException e) {
			return s;
		}
	}

	public boolean isEmailValid(String email) {
		String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if (matcher.matches())
			return true;
		else
			return false;
	}
}