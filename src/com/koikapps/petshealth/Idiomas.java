package com.koikapps.petshealth;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sun.tools.apt.Main;

public class Idiomas extends Activity {

	SharedPreferences prefs;
	Button boton_espanol, boton_english, boton_japones;
	private AdView adView;
	private static final String TEST_DEVICE_ID = "4df7f188777d1165";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_idiomas);
		boton_espanol = (Button) findViewById(R.id.button_espanol);
		boton_english = (Button) findViewById(R.id.button_english);
		boton_japones = (Button) findViewById(R.id.button_japones);

		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(TEST_DEVICE_ID).build();
		adView.loadAd(adRequest);

		boton_espanol.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "es");
				editor.commit();

				String languageToLoad2 = "es";
				Locale locale2 = new Locale(languageToLoad2);
				Locale.setDefault(locale2);
				Configuration config2 = new Configuration();
				config2.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config2,
						getBaseContext().getResources().getDisplayMetrics());

				Intent configuracion_intent = new Intent();
				configuracion_intent.setClass(Idiomas.this,
						MainActivity.class);
				startActivity(configuracion_intent);
			}
		});
		boton_english.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "en");
				editor.commit();

				String languageToLoad2 = "en";
				Locale locale2 = new Locale(languageToLoad2);
				Locale.setDefault(locale2);
				Configuration config2 = new Configuration();
				config2.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config2,
						getBaseContext().getResources().getDisplayMetrics());

				Intent configuracion_intent = new Intent();
				configuracion_intent.setClass(Idiomas.this,
						MainActivity.class);
				startActivity(configuracion_intent);
			}
		});
		boton_japones.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "ja");
				editor.commit();

				String languageToLoad2 = "ja";
				Locale locale2 = new Locale(languageToLoad2);
				Locale.setDefault(locale2);
				Configuration config2 = new Configuration();
				config2.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config2,
						getBaseContext().getResources().getDisplayMetrics());

				Intent configuracion_intent = new Intent();
				configuracion_intent.setClass(Idiomas.this,
						MainActivity.class);
				startActivity(configuracion_intent);
			}
		});

	}

}
