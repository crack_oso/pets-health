package com.koikapps.petshealth;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {

	Button registro, iniciarSesion;
	SharedPreferences prefs;
	private AdView adView;
	private static final String TEST_DEVICE_ID = "4df7f188777d1165";
	int mascotas;
	String nombre;
	Context contexto = this;
	ListView list;
	TextView nombre_list, especie_list;
	ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	private static final String TAG_NOMBRE = "nombre";
	private static final String TAG_RAZA = "raza";
	private static final String TAG_ESPECIE = "especie";
	private static final String TAG_ID_MASCOTA = "id";
	private static final String TAG_IMAGEN = "item_list_imagen";
	int elementosLista = 0;
	ListAdapter adapter = null;
	String usuarioID;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		hayInternet();
	}

	public void hayInternet() {
		Boolean hayInternet = new HayInternet().revisarAhora(this);
		if (hayInternet) {
			prefs = getSharedPreferences("preferencias", 0);
		}
	}

	public void onStart() {
		super.onStart();
		hayInternet();
		idioma();
		remember();
	}

	private void idioma() {
		String idioma = prefs.getString("idioma", "ninguno");
		if (idioma.equals("ninguno")) {
			String idiomaLocal = Locale.getDefault().getDisplayLanguage();
			Log.i("Idioma de sistema", Locale.getDefault().getDisplayLanguage());

			if (idiomaLocal.equals("español")) {
				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "es");
				editor.commit();
			} else if (idiomaLocal.equals("English")) {
				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "en");
				editor.commit();
			} else if (idiomaLocal.equals("ja")) {
				prefs = getSharedPreferences("preferencias", 0);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString("idioma", "ja");
				editor.commit();
			} else if (idiomaLocal.equals("русский")) {
				// Este es ruso
				Intent intentIdiomas = new Intent();
				intentIdiomas.setClass(MainActivity.this, Idiomas.class);
				startActivity(intentIdiomas);
			} else {
				Intent intentIdiomas = new Intent();
				intentIdiomas.setClass(MainActivity.this, Idiomas.class);
				startActivity(intentIdiomas);
			}
		}
	}

	private void remember() {
		boolean thisRemember = prefs.getBoolean("login", false);
		if (thisRemember) {
			pantallaPrincipal();
			/*
			 * Intent intent = new Intent(); intent.setClass(MainActivity.this,
			 * PantallaPrincipal.class); startActivity(intent);
			 */

		} else {
			setContentView(R.layout.activity_main);

			registro = (Button) findViewById(R.id.Registrarse);
			registro.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, Registro.class);
					startActivity(intent);
				}
			});

			iniciarSesion = (Button) findViewById(R.id.IniciarSesion);
			iniciarSesion.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, IniciarSesion.class);
					startActivity(intent);
				}
			});
		}

	}

	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		// super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_principal, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public void pantallaPrincipal() {
		setContentView(R.layout.activity_pantallaprincipal);

		Button mascota = (Button) findViewById(R.id.nuevaMascota);

		prefs = getSharedPreferences("preferencias", 0);

		mascota.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent MascotaIntent = new Intent();
				MascotaIntent.setClass(MainActivity.this, NuevaMascota.class);
				startActivity(MascotaIntent);
			}
		});
		oslist.clear();
		new JsonMascotas().execute();

		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(TEST_DEVICE_ID).build();
		adView.loadAd(adRequest);
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.cerrarSesion:
			prefs = getSharedPreferences("preferencias", 0);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean("login", false);
			editor.putString("usuario_id", "");
			editor.putString("usuario", "");
			editor.putString("password", "");
			editor.putString("currentMascotaID", "");
			editor.commit();
			Intent intent2 = new Intent();
			intent2.setClass(MainActivity.this, MainActivity.class);
			startActivity(intent2);
			break;
		case R.id.action_share:
			Intent sharingIntent = new Intent(
					android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			String shareBody = getString(R.string.compartirMensaje);
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					getString(R.string.compartirAsunto));
			sharingIntent
					.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			startActivity(Intent.createChooser(sharingIntent,
					getString(R.string.compartirVia)));
			break;
		case R.id.configuracion:
			Intent intent3 = new Intent();
			intent3.setClass(MainActivity.this, Configuracion.class);
			startActivity(intent3);
			break;
		}

		return false;
	}

	public class JsonMascotas extends AsyncTask<String, String, JSONArray> {

		String url = "http://koikapps.com/petshealth/web/usuario/mascotasJson/usuario_id/"
				+ prefs.getString("usuario_id", null);
		ProgressDialog pDialog;
		Bitmap bitmap;

		protected void onPreExecute() {
			super.onPreExecute();
			especie_list = (TextView) findViewById(R.id.item_especie);
			nombre_list = (TextView) findViewById(R.id.item_nombre);

			pDialog = new ProgressDialog(MainActivity.this,
					R.style.progress_bar_style);
			pDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			pDialog.setTitle(null);
			pDialog.setMessage(getString(R.string.CargandoTexto));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected JSONArray doInBackground(String... args) {

			JSONArray json;
			try {
				DefaultHttpClient cliente = new DefaultHttpClient(
						new BasicHttpParams());
				HttpGet get = new HttpGet(url);
				get.setHeader("Content-type", "application/json");
				InputStream is = null;
				String result = null;
				HttpResponse response = cliente.execute(get);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);

				StringBuilder stringBuilder = new StringBuilder();
				String line = null;

				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line + "\n");
				}
				result = stringBuilder.toString();
				json = new JSONArray(result);
				return json;
			} catch (Exception e) {
				e.getStackTrace();
				Log.i("error al ejecutar: ", "false");
			}

			return null;
		}

		@Override
		protected void onPostExecute(JSONArray json) {
			pDialog.dismiss();
			list = (ListView) findViewById(R.id.listaMascotas);
			final ImageView view = (ImageView) findViewById(R.id.item_list_imagen);
			usuarioID = prefs.getString("usuario_id", null);

			try {
				mascotas = json.length();
				if (mascotas > 0) {
					for (int i = 0; i < json.length(); i++) {
						JSONObject c = json.getJSONObject(i);

						String nomMascota = c.getString(TAG_NOMBRE);
						String espMascota = c.getString(TAG_ESPECIE);
						String razaMascota = c.getString(TAG_RAZA);
						String IDMascota = c.getString(TAG_ID_MASCOTA);

						if (razaMascota.length() > 0 || espMascota.length() > 0) {
							razaMascota = razaMascota + " (" + espMascota + ")";
						}

						final String thumb_u = "http://koikapps.com/uploadImages/images/"
								+ usuarioID + "/" + IDMascota + ".jpg";

						//Checar esto, a ver si después de descargar el bitmap se puede hacer un this.imageview.setImageView(bitmap);
						Thread thread = new Thread() {
							@Override
							public void run() {
								try {

									bitmap = getBitmap(thumb_u, view);

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						};

						thread.start();

						HashMap<String, String> map = new HashMap<String, String>();
						map.put(TAG_NOMBRE, nomMascota);
						map.put(TAG_RAZA, razaMascota);
						map.put(TAG_ID_MASCOTA, IDMascota);
						//map.put(TAG_IMAGEN, bitmap.toString());

						oslist.add(map);

						//Agarrar el adaptador y modificarlo para que el cambie la vista
						
						adapter = new SimpleAdapter(MainActivity.this, oslist,
								R.layout.list_item_layout, new String[] {
										TAG_NOMBRE, TAG_RAZA, TAG_ID_MASCOTA
										//,TAG_IMAGEN 
										}, new int[] {
										R.id.item_nombre, R.id.item_especie,
										R.id.item_id_mascota
										//,R.id.item_list_imagen 
										});
						list.setAdapter(adapter);
						list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {

								prefs = getSharedPreferences("preferencias", 0);
								SharedPreferences.Editor editor = prefs.edit();
								editor.putString("currentMascotaID", oslist
										.get(position).get(TAG_ID_MASCOTA)
										.toString());
								editor.commit();

								Intent mascota_intent = new Intent();
								mascota_intent.setClass(MainActivity.this,
										MiMascota.class);
								startActivity(mascota_intent);
							}
						});
					}
				} else {
					Log.i("Aún no tiene mascotas", "true");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public Bitmap getBitmap(String bitmapUrl, ImageView view) {
			try {
				URL url = new URL(bitmapUrl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setDoInput(true);
				conn.connect();
				InputStream input = conn.getInputStream();
				Bitmap mybitmap = BitmapFactory.decodeStream(input);
				view.setImageBitmap(mybitmap);
				
				return mybitmap;

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

	}
}