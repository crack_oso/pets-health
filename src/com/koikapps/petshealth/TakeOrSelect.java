package com.koikapps.petshealth;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TakeOrSelect extends Activity implements OnClickListener {

	private Button select;
	private Button take;
	private Intent intent;
	private static final int CODE_SELECT = 1;
	private static final int CODE_TAKE = 2;
	String upLoadServerUri = "http://koikapps.com/uploadImages/upload_image_android.php";
	int serverResponseCode = 0;
	TextView path_tv;
	String pathToImage;
	Uri uri;
	String uploadFilePath = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/";
	final String uploadFileName = "pumas.jpg";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_or_select);

		take = (Button) this.findViewById(R.id.take);
		select = (Button) this.findViewById(R.id.select);
		take.setOnClickListener(this);
		select.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v == select) {
			// open the image gallery intent
			intent = new Intent(Intent.ACTION_PICK,
					MediaStore.Images.Media.INTERNAL_CONTENT_URI);
			startActivityForResult(intent, CODE_SELECT);
		}

		if (v == take) {
			// open the camera intent
			intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, CODE_TAKE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (data != null) {
			uri = data.getData();
			pathToImage = getRealPathFromURI(uri);

			if (resultCode != Activity.RESULT_OK) {
				return;
			}
			// if an image was selected get data from uri
			if (requestCode == CODE_SELECT) {
				Toast.makeText(TakeOrSelect.this,
						"La ubicacion es: " + pathToImage, Toast.LENGTH_LONG)
						.show();
				Log.i("url a la imagen", pathToImage.toString());
				Intent intentSelect = new Intent();
				intentSelect.setClass(TakeOrSelect.this, Cropper.class);
				intentSelect.putExtra("URL", pathToImage);
				startActivity(intentSelect);
			}
			// if an image was made with the camera, get the bitmap data
			if (requestCode == CODE_TAKE && resultCode == Activity.RESULT_OK) {
				Toast.makeText(TakeOrSelect.this,
						"La ubicacion es: " + pathToImage, Toast.LENGTH_LONG)
						.show();
				Intent intentTake = new Intent();
				intentTake.setClass(TakeOrSelect.this, Cropper.class);
				intentTake.putExtra("URL", pathToImage);
				startActivity(intentTake);

			}
		}
		return;

	}

	private String getRealPathFromURI(Uri contentURI) {
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file
								// path
			return contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(idx);
		}
	}

}
