package com.koikapps.petshealth;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class DetalleVacunas extends Activity {
	private AdView adView;
	private static final String TEST_DEVICE_ID = "4df7f188777d1165";
	int mascotas;
	String nombre;
	CharSequence vacunas = " ";
	SharedPreferences prefs;
	DetalleVacunas contexto = this;
	ListView list;
	TextView nombre_list, especie_list;
	ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	private static final String TAG_NOMBRE = "nombre";
	private static final String TAG_FECHA = "fecha";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_detalles);

		new JsonDetalleVacunas().execute();

		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(TEST_DEVICE_ID).build();
		adView.loadAd(adRequest);
	}

	public class JsonDetalleVacunas extends
			AsyncTask<String, String, JSONArray> {
		SharedPreferences prefs = getSharedPreferences("preferencias", 0);

		String url = "http://koikapps.com/petshealth/web/mascota/vacunas/id/"
				+ prefs.getString("currentMascotaID", null);
		ProgressDialog pDialog;

		protected void onPreExecute() {
			super.onPreExecute();
			especie_list = (TextView) findViewById(R.id.item_especie);
			nombre_list = (TextView) findViewById(R.id.item_nombre);
			
			pDialog = new ProgressDialog(DetalleVacunas.this,
					R.style.progress_bar_style);
			pDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			pDialog.setTitle(null);
			pDialog.setMessage(getString(R.string.CargandoTexto));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected JSONArray doInBackground(String... args) {

			JSONArray json;
			try {
				DefaultHttpClient cliente = new DefaultHttpClient(
						new BasicHttpParams());
				HttpGet get = new HttpGet(url);
				get.setHeader("Content-type", "application/json");
				InputStream is = null;
				String result = null;
				HttpResponse response = cliente.execute(get);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);

				StringBuilder stringBuilder = new StringBuilder();
				String line = null;

				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line + "\n");
				}
				result = stringBuilder.toString();
				json = new JSONArray(result);
				return json;
			} catch (Exception e) {
				e.getStackTrace();
				Log.i("error al ejecutar: ", "false");
			}
			return null;
		}

		@Override
		protected void onPostExecute(JSONArray json) {
			pDialog.dismiss();

			Log.i("JSON Vacunas", "" + json);

			try {
				mascotas = json.length();
				if (mascotas > 0) {
					JSONObject c = json.getJSONObject(0);
					JSONArray arregloVacunas = c.getJSONArray("Vacuna");

					JSONObject va;
					String nomVacuna = " ", fechaVacuna = " ";
					for (int j = 0; j < arregloVacunas.length(); j++) {
						va = arregloVacunas.getJSONObject(j);
						nomVacuna = va.getString(TAG_NOMBRE);
						fechaVacuna = va.getString(TAG_FECHA);
						Log.i("valor de la lista: ", nomVacuna+ " "+fechaVacuna);

				        SimpleDateFormat dateFormat = new SimpleDateFormat(
				                "yyyy-MM-dd");
				        Date myDate = null;
				        try {
				            myDate = dateFormat.parse(fechaVacuna);

				        } catch (ParseException e) {
				            e.printStackTrace();
				        }

				        SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd-yy");
				        String finalDate = timeFormat.format(myDate);

				        HashMap<String, String> map = new HashMap<String, String>();
						map.put(TAG_NOMBRE, nomVacuna);
						map.put(TAG_FECHA, finalDate.toString());
						oslist.add(map);
					}
					list = (ListView) findViewById(R.id.listaVacunas);

					ListAdapter adapter = new SimpleAdapter(
							DetalleVacunas.this, oslist,
							R.layout.list_item_details, new String[] {
									TAG_NOMBRE, TAG_FECHA }, new int[] {
									R.id.item_detalle_nombre,
									R.id.item_detalle_fecha });

					list.setAdapter(adapter);
				} else {
					Log.i("Aún no tiene mascotas", "true");
					TextView notiene = (TextView) findViewById(R.id.item_detalle_nombre);
					notiene.setText(R.string.noTiene + " " + R.string.vacunas +" "+ R.string.registradas);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
