package com.koikapps.petshealth;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edmodo.cropper.CropImageView;

public class Cropper extends Activity {

	// Static final constants
	private static final int DEFAULT_ASPECT_RATIO_VALUES = 10;
	private static final int ROTATE_NINETY_DEGREES = 90;
	private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
	private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";
	ProgressDialog dialog;
	Uri uri;

	// Instance variables
	private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
	private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;
	String upLoadServerUri = "http://koikapps.com/uploadImages/upload_image_android.php";
	int serverResponseCode = 0;
	String filePath = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/Cropper";
	String uploadImage = null;
	Bitmap croppedImage;
	static Bitmap bmRotated = null;
	String photoPath, fileName;

	// Saves the state upon rotating the screen/restarting the activity
	@Override
	protected void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
		bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
	}

	// Restores the state upon rotating the screen/restarting the activity
	@Override
	protected void onRestoreInstanceState(Bundle bundle) {
		super.onRestoreInstanceState(bundle);
		mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
		mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
	}

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cropper);

		uploadImage = getIntent().getExtras().getString("URL").toString();
		// Initialize components of the app
		final CropImageView cropImageView = (CropImageView) findViewById(R.id.CropImageView);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		Bitmap bitmapOriginal = BitmapFactory.decodeFile(uploadImage);
		int ancho = bitmapOriginal.getWidth() / metrics.widthPixels;
		int alto = bitmapOriginal.getHeight() / metrics.heightPixels;
		if (ancho == 0) {
			ancho = 1;
		}
		if (alto == 0) {
			alto = 1;
		}

		// exif.getAttribute(ExifInterface.TAG_ORIENTATION));
		Bitmap bitmapsimplesize = Bitmap.createScaledBitmap(bitmapOriginal,
				bitmapOriginal.getWidth() / 5, bitmapOriginal.getHeight() / 5,
				true);
		bitmapOriginal.recycle();
		cropImageView.setImageBitmap(bitmapsimplesize);

		// Sets sliders to be disabled until fixedAspectRatio is set

		// Sets the rotate button
		final Button rotateButton = (Button) findViewById(R.id.Button_rotate);
		rotateButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				cropImageView.rotateImage(ROTATE_NINETY_DEGREES);
			}
		});

		// Sets fixedAspectRatio

		// Sets initial aspect ratio to 10/10, for demonstration purposes
		cropImageView.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES,
				DEFAULT_ASPECT_RATIO_VALUES);

		// Sets aspectRatioX
		// Sets aspectRatioY
		// Sets up the Spinner

		final Button cropButton = (Button) findViewById(R.id.Button_crop);
		cropButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				croppedImage = cropImageView.getCroppedImage();
				cropImageView.setImageBitmap(croppedImage);
				saveBitmap();
				dialog = ProgressDialog.show(Cropper.this, "Progress",
						"Uploading image...", true);
				new Thread(new Runnable() {
					public void run() {
						uploadFile(photoPath);
					}
				}).start();

				Log.i("Ruta a la imagen final",
						"absolute path: " + photoPath.toString());
			}
		});
	}

	public void saveBitmap() {
		try {
			File dir = new File(filePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HHmmss");
			String currentDateandTime = sdf.format(new Date());
			long time = new Date().getTime() - 10000;
			OutputStream fOut = null;
			File file = new File(filePath, currentDateandTime + ".jpg");
			file.setLastModified(time);
			file.createNewFile();

			fOut = new FileOutputStream(file);

			// 100 means no compression, the lower you go, the stronger the
			// compression
			croppedImage.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			fOut.flush();
			fOut.close();
			MediaStore.Images.Media.insertImage(getContentResolver(),
					file.getAbsolutePath(), file.getName(), file.getName());

			photoPath = file.getPath();
			fileName = file.getName() + ".jpg";

			Log.i("nombre archivo", "File Name: " + fileName + "\n path: "
					+ photoPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Sets the font on all TextViews in the ViewGroup. Searches recursively for
	 * all inner ViewGroups as well. Just add a check for any other views you
	 * want to set as well (EditText, etc.)
	 */
	public void setFont(ViewGroup group, Typeface font) {
		int count = group.getChildCount();
		View v;
		for (int i = 0; i < count; i++) {
			v = group.getChildAt(i);
			if (v instanceof TextView || v instanceof EditText
					|| v instanceof Button) {
				((TextView) v).setTypeface(font);
			} else if (v instanceof ViewGroup)
				setFont((ViewGroup) v, font);
		}
	}

	public int uploadFile(String sourceFileUri) {

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		SharedPreferences prefs = getSharedPreferences("preferencias", 0);
		String currentPet, currentUsuario;

		currentPet = prefs.getString("currentMascotaID", null);
		currentUsuario = prefs.getString("usuario_id", null);

		if (!sourceFile.isFile()) {

			dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + photoPath);

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				// conn.setRequestProperty("uploaded_file", fileName);
				conn.setRequestProperty("uploaded_file", photoPath);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ currentUsuario
						+ "-"
						+ currentPet
						+ ".jpg"
						+ "\""
						+ lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {

					runOnUiThread(new Runnable() {
						public void run() {

							String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
									+ " koikapps.com/uploadImages/images/"
									+ uploadImage;

							Toast.makeText(Cropper.this,
									"File Upload Complete.", Toast.LENGTH_SHORT)
									.show();
						}
					});
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(Cropper.this, "MalformedURLException",
								Toast.LENGTH_SHORT).show();
					}
				});

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				dialog.dismiss();
				e.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(Cropper.this,
								"Got Exception : see logcat ",
								Toast.LENGTH_SHORT).show();
					}
				});
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}
			
			Intent intentTake = new Intent();
			intentTake.setClass(Cropper.this, MiMascota.class);
			startActivity(intentTake);

			dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}
}
