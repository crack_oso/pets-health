package com.koikapps.petshealth;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class NuevaMascota extends Activity {

	public String uriAPI = "http://koikapps.com/petshealth/web/insert/pet.php";

	private Button continuar;
	private ImageView calendarioNuevaMascota;
	public TextView nombre_mascota, fechaNacimiento, especie, raza,
			dateMascota;
	Spinner sexo, tamano;
	private int year, month, day;
	String fecha;
	static final int DATE_DIALOG_ID = 999;
	SharedPreferences prefs;
	public static String USUARIO_ID = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nuevamascota);

		prefs = getSharedPreferences("preferencias", 0);
		USUARIO_ID = prefs.getString("usuario_id", null);

		nombre_mascota = (TextView) findViewById(R.id.nombre_mascota);
		fechaNacimiento = (TextView) findViewById(R.id.cumple_mascota);
		especie = (TextView) findViewById(R.id.especia_mascota);
		raza = (TextView) findViewById(R.id.raza_mascota);
		continuar = (Button) findViewById(R.id.button_continuar);
		dateMascota = (TextView) findViewById(R.id.cumple_mascota);

		continuar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (nombre_mascota.length() <= 0) {
					Toast.makeText(getApplication(), R.string.faltaNombre,
							Toast.LENGTH_SHORT).show();
				} else {
					sendPostRequest(uriAPI);
				}
			}

		});
		tamano = (Spinner) findViewById(R.id.tamano_mascota);
		ArrayAdapter<CharSequence> contenido_tamano = ArrayAdapter
				.createFromResource(this, R.array.tamano,
						android.R.layout.simple_dropdown_item_1line);
		contenido_tamano.setDropDownViewResource(R.layout.spinner_tamano);
		tamano.setAdapter(contenido_tamano);

		sexo = (Spinner) findViewById(R.id.sexo_mascota);
		ArrayAdapter<CharSequence> contenido_sexo = ArrayAdapter
				.createFromResource(this, R.array.sexo,
						android.R.layout.simple_dropdown_item_1line);
		contenido_sexo.setDropDownViewResource(R.layout.spinner_sexo);
		sexo.setAdapter(contenido_sexo);

		calendarioNuevaMascota = (ImageView) findViewById(R.id.calendarioNuevaMascota);
		calendarioNuevaMascota.setOnClickListener(new OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
		setCurrentDateOnView();

	}

	public void setCurrentDateOnView() {

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		StringBuilder currentDate = new StringBuilder().append(month + 1)
				.append("-").append(day).append("-").append(year);
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date myDate = null;
		try {
			myDate = dateFormat.parse(currentDate.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM-dd-yyyy");
		String finalDate = timeFormat.format(myDate);
		dateMascota.setText(finalDate.toString());
		fecha = finalDate;

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			StringBuilder selectedDate = new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year);

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

			Date myDate = null;
			try {
				myDate = dateFormat.parse(selectedDate.toString());

			} catch (ParseException e) {
				e.printStackTrace();
			}
			SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM-dd-yyyy");
			String date = timeFormat.format(myDate);
			fecha = date;

			dateMascota.setText(date);

		}
	};

	public void sendPostRequest(final String URL) {

		class MyPost extends AsyncTask<Void, Void, Void> {

			ProgressDialog pDialog;

			protected void onPreExecute() {
				super.onPreExecute();

				pDialog = new ProgressDialog(NuevaMascota.this,
						R.style.progress_bar_style);
				pDialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));
				pDialog.setTitle(null);
				pDialog.setMessage(getString(R.string.CargandoTexto));
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
			}

			@Override
			protected Void doInBackground(Void... arg0) {
				// Create a new HttpClient and Post Header
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(URL);

				try {
					// Add your data
					Date lastDate = null;
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"MMMM-dd-yyyy");
					try {
						lastDate = dateFormat.parse(fechaNacimiento.getText()
								.toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					SimpleDateFormat timeFormat = new SimpleDateFormat(
							"yyyy-MM-dd");
					String dateSended = timeFormat.format(lastDate);

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("usuario_id",
							USUARIO_ID));
					nameValuePairs.add(new BasicNameValuePair("nombre",
							nombre_mascota.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair(
							"fechanacimiento", dateSended.toString()));
					nameValuePairs.add(new BasicNameValuePair("especie",
							especie.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("raza", raza
							.getText().toString()));
					if (tamano.getSelectedItemPosition() == 0) {
						nameValuePairs.add(new BasicNameValuePair("tamano", "0"));
					} else if (tamano.getSelectedItemPosition() == 1) {
						nameValuePairs.add(new BasicNameValuePair("tamano", "1"));
					} else if (tamano.getSelectedItemPosition() == 2) {
						nameValuePairs.add(new BasicNameValuePair("tamano", "2"));
					}
					if (sexo.getSelectedItemPosition() == 0) {
						nameValuePairs.add(new BasicNameValuePair("sexo", "0"));
					} else if (sexo.getSelectedItemPosition() == 1) {
						nameValuePairs.add(new BasicNameValuePair("sexo", "1"));
					}

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					// Execute HTTP Post Request
					httpclient.execute(httppost);
					// Log.v("Post Status","Code: "+response.getStatusLine().getStatusCode());
				} catch (ClientProtocolException e) {
					Log.i("response", "nope" + e);
				} catch (IOException e) {
					Log.i("response", "nope" + e);
				}

				return null;

			}

			protected void onPostExecute(Void result) {
				pDialog.dismiss();
				super.onPostExecute(result);
				Toast.makeText(getApplicationContext(),
						R.string.seHaGuardadoTu, Toast.LENGTH_LONG).show();
				Intent intent2 = new Intent();
				intent2.setClass(NuevaMascota.this, MainActivity.class);
				startActivity(intent2);

			}
		}

		MyPost sendPostReqAsyncTask = new MyPost();
		sendPostReqAsyncTask.execute();

	}
}
