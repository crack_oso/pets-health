package com.koikapps.petshealth;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Configuracion extends Activity {

	String APP_NAME = "com.koikapps.petshealth";
	Button boton_idioma, boton_acerca, boton_rateus;
	private AdView adView;
	private static final String TEST_DEVICE_ID = "4df7f188777d1165";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);
		boton_idioma = (Button) findViewById(R.id.button_idiomas);
		boton_acerca = (Button) findViewById(R.id.button_acerca);
		boton_rateus = (Button) findViewById(R.id.button_rateus);

		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(TEST_DEVICE_ID).build();
		adView.loadAd(adRequest);
		
		boton_idioma.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent configuracion_intent = new Intent();
				configuracion_intent
						.setClass(Configuracion.this, Idiomas.class);
				startActivity(configuracion_intent);
			}
		});

		boton_acerca.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent configuracion_intent = new Intent();
				configuracion_intent.setClass(Configuracion.this, Acerca.class);
				startActivity(configuracion_intent);
			}
		});
		boton_rateus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("market://details?id=" + APP_NAME)));
			}
		});

	}

}
