package com.koikapps.petshealth;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MiMascota extends Activity implements OnClickListener {

	private AdView adView;
	private static final String TEST_DEVICE_ID = "4df7f188777d1165";
	int mascotas;
	String nombre, fechaNacimiento, especie, raza, tamano, sexo, mascota_id;
	CharSequence vacunas = " ", enfermedad = " ", alergia = " ";
	ProgressDialog dialog, dialogDelete;
	ImageView ImgVacunas, ImgEnfermedades, ImgAlergias, imagenMascota;
	Context contexto = this;
	String currentPet, usuarioID;
	Uri uri = null;
	Bitmap image;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_mi_mascota);

		final SharedPreferences prefs = getSharedPreferences("preferencias", 0);
		new JsonMiMascota().execute();
		ImgVacunas = (ImageView) findViewById(R.id.masVacunas);
		ImgEnfermedades = (ImageView) findViewById(R.id.masEnfermedades);
		ImgAlergias = (ImageView) findViewById(R.id.masAlergias);
		imagenMascota = (ImageView) findViewById(R.id.imagenMascota);
		ImgVacunas.setOnClickListener(this);
		ImgEnfermedades.setOnClickListener(this);
		ImgAlergias.setOnClickListener(this);
		imagenMascota.setOnClickListener(this);


		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					currentPet = prefs.getString("currentMascotaID", null);
					usuarioID = prefs.getString("usuario_id", null);

					String thumb_u = "http://koikapps.com/uploadImages/images/"
							+ usuarioID + "/" + currentPet + ".jpg";
					Log.i("url ", thumb_u);

					Bitmap bitmap = getBitmap(thumb_u);
					imagenMascota.setImageBitmap(bitmap);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		thread.start();
		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(TEST_DEVICE_ID).build();
		adView.loadAd(adRequest);

	}

	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		// super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_mi_mascota, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public Bitmap getBitmap(String bitmapUrl) {
		try {
			URL url = new URL(bitmapUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.connect();
			InputStream input = conn.getInputStream();
			Bitmap mybitmap = BitmapFactory.decodeStream(input);
			return mybitmap;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_delete:
			AlertDialog diaBox = AskOption();
			diaBox.show();
			break;
		}
		return false;
	}

	private AlertDialog AskOption() {
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
				// set message, title, and icon
				.setMessage(R.string.quieresEliminar)
				.setTitle(R.string.Eliminar)
				.setIcon(R.drawable.ic_action_delete)
				.setPositiveButton(R.string.Eliminar,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int whichButton) {
								Log.i("CurrentPet: ", currentPet);
								new Thread(new Runnable() {
									public void run() {

										try {
											DefaultHttpClient cliente = new DefaultHttpClient(
													new BasicHttpParams());
											HttpGet get = new HttpGet(
													"http://koikapps.com/petshealth/web/frontend_dev.php/mascota/delete/id/"
															+ currentPet);
											get.setHeader("Content-type",
													"application/json");
											InputStream is = null;
											String result = null;
											HttpResponse response = cliente
													.execute(get);
											HttpEntity entity = response
													.getEntity();
											is = entity.getContent();
											BufferedReader reader = new BufferedReader(
													new InputStreamReader(is,
															"iso-8859-1"), 8);

											StringBuilder stringBuilder = new StringBuilder();
											String line = null;

											while ((line = reader.readLine()) != null) {
												stringBuilder.append(line
														+ "\n");
											}
											result = stringBuilder.toString();
											Log.i("Success!!",
													"Se ha eliminado correctamente");

											Intent MainIntent = new Intent();
											MainIntent.setClass(MiMascota.this,
													MainActivity.class);
											startActivity(MainIntent);

										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}).start();

							}

						})

				.setNegativeButton(R.string.Cancelar,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();
		return myQuittingDialogBox;

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.masVacunas:
			Intent vacuna_intent = new Intent();
			vacuna_intent.setClass(MiMascota.this, MasVacunas.class);
			vacuna_intent.putExtra("mascota_id", mascota_id);
			startActivity(vacuna_intent);
			break;
		case R.id.masEnfermedades:
			Intent enfermedades_intent = new Intent();
			enfermedades_intent.setClass(MiMascota.this, MasEnfermedades.class);
			enfermedades_intent.putExtra("mascota_id", mascota_id);
			startActivity(enfermedades_intent);
			break;
		case R.id.masAlergias:
			Intent alergia_intent = new Intent();
			alergia_intent.setClass(MiMascota.this, MasAlergias.class);
			alergia_intent.putExtra("mascota_id", mascota_id);
			startActivity(alergia_intent);
			break;
		case R.id.imagenMascota:
			Intent imagen_intent = new Intent();
			imagen_intent.setClass(MiMascota.this, TakeOrSelect.class);
			imagen_intent.putExtra("mascota_id", mascota_id);
			startActivity(imagen_intent);
			break;
		}
	}

	public class JsonMiMascota extends AsyncTask<String, String, JSONArray> {

		String url = "http://koikapps.com/petshealth/web/mascota/json/id/";
		ProgressDialog pDialog;

		protected void onPreExecute() {
			super.onPreExecute();

			SharedPreferences prefs = getSharedPreferences("preferencias", 0);
			currentPet = prefs.getString("currentMascotaID", null);
			url = url + currentPet;

			pDialog = new ProgressDialog(MiMascota.this,
					R.style.progress_bar_style);
			pDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			pDialog.setTitle(null);
			pDialog.setMessage(getString(R.string.CargandoTexto));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected JSONArray doInBackground(String... args) {

			JSONArray json;
			try {
				DefaultHttpClient cliente = new DefaultHttpClient(
						new BasicHttpParams());
				HttpGet get = new HttpGet(url);
				get.setHeader("Content-type", "application/json");
				InputStream is = null;
				String result = null;
				HttpResponse response = cliente.execute(get);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);

				StringBuilder stringBuilder = new StringBuilder();
				String line = null;

				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line + "\n");
				}
				result = stringBuilder.toString();
				json = new JSONArray(result);
				return json;
			} catch (Exception e) {
				e.getStackTrace();
				Log.i("error al ejecutar: ", "false");
			}
			return null;
		}

		@Override
		protected void onPostExecute(JSONArray Arrayjson) {
			pDialog.dismiss();

			try {
				for (int h = 0; h < Arrayjson.length(); h++) {
					JSONObject json = Arrayjson.getJSONObject(h);

					nombre = json.getString("nombre");
					fechaNacimiento = json.getString("fechaNacimiento");
					especie = json.getString("especie");
					raza = json.getString("raza");
					tamano = json.getString("tamano");
					sexo = json.getString("sexo");
					mascota_id = json.getString("id");

					JSONArray arregloVacunas = json.getJSONArray("Vacuna");
					JSONObject va;
					for (int i = 0; i < arregloVacunas.length(); i++) {
						if (vacunas.equals(" ")) {
							vacunas = "";
						}
						va = arregloVacunas.getJSONObject(i);
						vacunas = vacunas + va.getString("vacuna") + "\n";

					}
					JSONArray arregloEnfermedades = json
							.getJSONArray("Enfermedad");
					JSONObject en;
					for (int j = 0; j < arregloEnfermedades.length(); j++) {
						if (enfermedad.equals(" ")) {
							enfermedad = "";
						}
						en = arregloEnfermedades.getJSONObject(j);
						enfermedad = enfermedad + en.getString("enfermedad")
								+ "\n";
					}
					JSONArray arregloAlergias = json.getJSONArray("Alergia");
					JSONObject al;
					for (int k = 0; k < arregloAlergias.length(); k++) {
						if (alergia.equals(" ")) {
							alergia = "";
						}
						al = arregloAlergias.getJSONObject(k);
						alergia = alergia + al.getString("alergia") + "\n";
					}
				}

			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				pDialog.dismiss();

				TextView nombreJSON = (TextView) findViewById(R.id.nombre_mascota_view_json);
				TextView fechaJSON = (TextView) findViewById(R.id.fecha_view_json);
				TextView especieJSON = (TextView) findViewById(R.id.especie_view_json);
				TextView razaJSON = (TextView) findViewById(R.id.raza_view_json);
				TextView tamanoJSON = (TextView) findViewById(R.id.tamano_view_json);
				TextView sexoJSON = (TextView) findViewById(R.id.sexo_view_json);
				TextView vacunasJSON = (TextView) findViewById(R.id.vacunas_view_json);
				TextView enfermedadesJSON = (TextView) findViewById(R.id.enfermedad_view_json);
				TextView alergiaJSON = (TextView) findViewById(R.id.alergias_view_json);

				vacunasJSON.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent vacunas_detalle = new Intent();
						vacunas_detalle.setClass(MiMascota.this,
								DetalleVacunas.class);
						startActivity(vacunas_detalle);
					}
				});
				enfermedadesJSON.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent enfermedad_detalle = new Intent();
						enfermedad_detalle.setClass(MiMascota.this,
								DetalleEnfermedades.class);
						startActivity(enfermedad_detalle);
					}
				});
				alergiaJSON.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent alergia_detalle = new Intent();
						alergia_detalle.setClass(MiMascota.this,
								DetalleAlergias.class);
						startActivity(alergia_detalle);
					}
				});
				if (sexo.equals("0")) {
					sexo = getString(R.string.Hembra);
				} else {
					sexo = getString(R.string.Macho);
				}
				if (tamano.equals("0")) {
					tamano = getString(R.string.Pequeno);
				} else if (tamano.equals("1")) {
					tamano = getString(R.string.Mediano);
				} else if (tamano.equals("2")) {
					tamano = getString(R.string.Grande);
				}
				if (fechaNacimiento.equals("null")) {
					fechaNacimiento = getString(R.string.desconocido);
				}
				nombreJSON.setText(nombre);
				fechaJSON.setText(fechaNacimiento);
				especieJSON.setText(especie);
				razaJSON.setText(raza);
				tamanoJSON.setText(tamano);
				sexoJSON.setText(sexo);
				vacunasJSON.setText(vacunas);
				enfermedadesJSON.setText(enfermedad);
				alergiaJSON.setText(alergia);
			} catch (Exception e) {
			}
		}
	}
}
