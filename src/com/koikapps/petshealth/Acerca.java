package com.koikapps.petshealth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Acerca extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_acerca);
		TextView correo = (TextView) findViewById(R.id.text_mail);
		TextView url = (TextView) findViewById(R.id.text_link);

		correo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
						"mailto", getString(R.string.AcercaMail), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.asuntoContacto));
				startActivity(Intent.createChooser(emailIntent, "Send email..."));
			}
		});

		url.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.petshealthapp.com"));
				startActivity(browserIntent);

			}
		});

	}

}
