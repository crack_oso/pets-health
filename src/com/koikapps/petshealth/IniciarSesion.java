package com.koikapps.petshealth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class IniciarSesion extends Activity {

	public EditText usuario, password;
	public String uriAPI = "http://koikapps.com/petshealth/web/usuario/login";
	HttpClient client;
	String Respuesta, encryptedPass;
	SharedPreferences prefs;
	ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_iniciarsesion);

		client = new DefaultHttpClient();
		usuario = (EditText) findViewById(R.id.usuario_sesion);
		password = (EditText) findViewById(R.id.password_sesion);

		Button continuar = (Button) findViewById(R.id.button_continuar);
		continuar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				pDialog = new ProgressDialog(IniciarSesion.this,
						R.style.progress_bar_style);
				pDialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));
				pDialog.setTitle(null);
				pDialog.setMessage(getString(R.string.CargandoTexto));
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
				new Read().execute();
			}
		});

	}

	public String loginMethod() throws ClientProtocolException, IOException {

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(uriAPI.toString());

		encryptedPass = password.getText().toString();

		encryptedPass = md5(encryptedPass);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("usuario", usuario.getText()
				.toString()));
		nameValuePairs.add(new BasicNameValuePair("pass", encryptedPass));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = httpclient.execute(httppost);
		if (response != null) {
			String line = "";
			InputStream inputstream = response.getEntity().getContent();
			line = convertStreamToString(inputstream);
			Log.i("La respuesta es: ", line.toString());
			Respuesta = line.toString();
		} else {
			Log.i("NO HAY RESPUESTA: ", "false");
		}
		Log.i("La respuesta line es: ", Respuesta.toString());

		pDialog.dismiss();
		return Respuesta.toString();
	}

	private String convertStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (Exception e) {
			Toast.makeText(this, "Stream Exception", Toast.LENGTH_SHORT).show();
		}
		return total.toString();
	}

	public class Read extends AsyncTask<String, Integer, String> {

		protected String doInBackground(String... params) {

			try {
				String login = loginMethod();
				return login.toString();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String result) {

			if (result.equals("[]")) {
				Toast.makeText(IniciarSesion.this, R.string.errorIniciarSesion,
						Toast.LENGTH_SHORT).show();

			} else if (result.length() == 0) {
				Toast.makeText(IniciarSesion.this, R.string.networkError,
						Toast.LENGTH_SHORT).show();
			} else {

				try {

					JSONArray json = new JSONArray(result);
					JSONObject c = json.getJSONObject(0);

					prefs = getSharedPreferences("preferencias", 0);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putBoolean("login", true);
					editor.putString("usuario_id", c.getString("id"));
					editor.putString("usuario", c.getString("nombre"));
					editor.putString("password", encryptedPass.toString());
					editor.commit();
					Intent intent = new Intent();
					intent.setClass(IniciarSesion.this, MainActivity.class);
					intent.putExtra("usuario_id", c.getString("id"));
					startActivity(intent);
				} catch (JSONException e) {
					e.printStackTrace();
					// Log.i("JSON error", "no se pudo mostrar el usuario");
				}

			}
		}
	}

	public String md5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

			return hexString.toString();
		}

		catch (NoSuchAlgorithmException e) {
			return s;
		}
	}

}